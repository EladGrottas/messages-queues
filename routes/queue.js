const express = require('express');

const router = express.Router();

const messagesQueue = {};

const ensureQueue = (name) => {
    if(!messagesQueue[name])
        messagesQueue[name] = [];
}

router.post('/:queueName', (req, res) => {
    const message = req.body;
    const { queueName } = req.params;
    if(!queueName){
        res.status(400).send("queueName must be provided");
        return;
    }
    if(message === null || message === undefined || !Object.keys(message).length) {
        res.status(400).send("message must be provided");
        return;
    }
    ensureQueue(queueName);
    messagesQueue[queueName].push(message);
    res.send("success");
  });
  
  router.get('/:queueName', (req, res) => {
    const { params: { queueName }, query: { timeout = 10 * 1000 } } = req;
    if(!queueName){
        res.status(400).send("queueName must be provided");
        return;
    }
    setTimeout(() => {
        ensureQueue(queueName);
        if(!messagesQueue[queueName].length)
            res.status(204).send();
        else {
            res.send(messagesQueue[queueName].shift())
        }
    }, timeout);
  });
  
  module.exports = router;