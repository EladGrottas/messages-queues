const express = require('express');
const bodyParser = require('body-parser');
const router = require('./routes/queue')

const app = express()
app.use(bodyParser.json())

const port = 3000

app.use('/api', router);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})